<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', function(){
    return view('table');
});

Route::get('/data-table', function(){
    return view('data-table');
});

//CRUD Cast

//Create Data Cast

//Masuk ke form tambah cast
Route::get('/cast/create', 'CastController@create');
//Kirim input ke table cast
Route::post('/cast', 'CastController@store');

//Read Data Cast

//Tampil Semua data kategori
Route::get('/cast', 'CastController@index');
//Detail cast berdasar id
Route::get('/cast/{cast_id}', 'CastController@show');

//update data cast

//masuk ke form cast berdasar id
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data cast berdasar id
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete data cast

//Delete data cast berdasar id
Route::delete('/cast/{cast_id}', 'CastController@destroy');