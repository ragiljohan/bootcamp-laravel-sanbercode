@extends('layout.master')

@section('judul')
    Update Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama</label>
      <input type="text" name="nama" value="{{old('nama', $cast->nama)}}" class="form-control">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Umur</label>
      <input type="number" name="umur" value="{{old('umur', $cast->umur)}}" class="form-control" >
    </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label >Bio</label>
      <textarea name="bio" id="" cols="30" rows="10" class="form-control">{{old('bio', $cast->bio)}}</textarea>
    </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection