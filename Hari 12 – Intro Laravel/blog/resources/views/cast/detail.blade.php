@extends('layout.master')

@section('judul')
    Detail Kategori
@endsection
@section('content')

<h1 class="text-primary">{{$cast->nama}} ({{$cast->umur}} Tahun)</h1>
<p>{{$cast->bio}}</p>
<a href = "/cast" class="btn btn-secondary btn-sm mb-4">Kembali</a>
@endsection