@extends('layout.master')

@section('judul')
    Register
@endsection
@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
    <label>First name:</label><br><br>
    <input type="text" name="first_name"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="last_name"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>
    <label>Nationality:</label><br><br>
    <select name="Nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singaporeans">Singaporeans</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="language">Bahasa Indonesia<br>
    <input type="checkbox" name="language">English<br>
    <input type="checkbox" name="language">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection